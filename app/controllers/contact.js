import Ember from 'ember';

export default Ember.Controller.extend({
  // emailAddress: '',
  // textAddress:'',
  emailPreenchido: Ember.computed.notEmpty('emailAddress'),
  emailValido: Ember.computed.match('emailAddress', /^.+@.+\..+$/),
  textValido: Ember.computed.gte('textAddress.length', 5),
  isValid: Ember.computed.and('emailPreenchido', 'emailValido', 'textValido'),
  isDisabled: Ember.computed.not('isValid'),
  actions: {
    saveMessage(){
      const email = this.get('emailAddress');
      const text = this.get('textAddress');
      const newMessage = this.store.createRecord('contact',{email:email,text:text});

      newMessage.save();
      this.set('responseMessage', `Mensagem enviada com sucesso! Seu email:${this.get('emailAddress')} e sua mensagem: ${this.get('textAddress')}`);
      this.set('emailAddress', '');
      this.set('textAddress','');
    }

  }

});
